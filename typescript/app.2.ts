//exClasse: 01
class Spacecraft{
    constructor(public propulsor: string){}

    jumpIntoHyperspace(){
        console.log(`Entering hyperspace wiht ${this.propulsor}`)
    }
}

let ship = new Spacecraft('hyperdriver')
ship.jumpIntoHyperspace()

//exClasse: 02
class MillenniumFalcon extends Spacecraft implements Containership {
    cargoContainers: number
    constructor(){
        //adicionando parametro padrao ao sapcecraft, classe pai
        super('hyderdrive')
        this.cargoContainers = 2
    }

    //sobreescrever o metodo
    jumpIntoHyperspace(){
        if(Math.random() >= 0.5){
            super.jumpIntoHyperspace()
        }else{console.log('Failed to jump into hyperspace')}
    }
}

let falcon = new MillenniumFalcon()
falcon.jumpIntoHyperspace()

//exinterface: 01
interface Containership{
    cargoContainers: number
    //opcional cargoContainers?: number
}

let goodForTheJob = (ship: Containership) => ship.cargoContainers >2

console.log(`Is falcon good for the jod? ${goodForTheJob(falcon)?'yes':'no'}`)