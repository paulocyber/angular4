import{Spacecraft,Containership} from './base-ships'

//exClasse: 02
export class MillenniumFalcon extends Spacecraft implements Containership {
    cargoContainers: number
    constructor(){
        //adicionando parametro padrao ao sapcecraft, classe pai
        super('hyderdrive')
        this.cargoContainers = 2
    }

    //sobreescrever o metodo
    jumpIntoHyperspace(){
        if(Math.random() >= 0.5){
            super.jumpIntoHyperspace()
        }else{console.log('Failed to jump into hyperspace')}
    }
}
