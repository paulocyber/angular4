//ex01
let message: string = "Help me, Obi-Wan Kenobi. You're my only hope!"
console.log(message)
//ex02
let episode: number = 4
console.log('This is episode: ' + 4)
episode = episode + 1
console.log("Next episode is " + episode)
//ex03
let favoriteDroid: string
favoriteDroid = 'BB-B'
console.log(favoriteDroid)
//ex04
let isEnoughToBeatMF = function (parsecs: number) : boolean{
    return parsecs < 12
}
let distance = 11
console.log(`Is ${distance} parsecs enough to beat Millennium Falcon? ${isEnoughToBeatMF(distance) ? 'YES':'NO'}`)
//ex05
let call = (name: string) => console.log(`Do you copy, ${name}`)
call('tese')
//ex06
function inc (speed: number, inc: number=1): number{
    return speed+inc
}
console.log(`inc (5,1) = ${inc(5,1)}`)
console.log(`inc (5) = ${inc(5)}`)