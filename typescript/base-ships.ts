class Spacecraft{
    constructor(public propulsor: string){}

    jumpIntoHyperspace(){
        console.log(`Entering hyperspace wiht ${this.propulsor}`)
    }
}

//exinterface: 01
interface Containership{
    cargoContainers: number
    //opcional cargoContainers?: number
}

export {Spacecraft, Containership}