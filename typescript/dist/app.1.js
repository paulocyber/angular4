//ex01
var message = "Help me, Obi-Wan Kenobi. You're my only hope!";
console.log(message);
//ex02
var episode = 4;
console.log('This is episode: ' + 4);
episode = episode + 1;
console.log("Next episode is " + episode);
//ex03
var favoriteDroid;
favoriteDroid = 'BB-B';
console.log(favoriteDroid);
//ex04
var isEnoughToBeatMF = function (parsecs) {
    return parsecs < 12;
};
var distance = 11;
console.log("Is " + distance + " parsecs enough to beat Millennium Falcon? " + (isEnoughToBeatMF(distance) ? 'YES' : 'NO'));
//ex05
var call = function (name) { return console.log("Do you copy, " + name); };
call('tese');
//ex06
function inc(speed, inc) {
    if (inc === void 0) { inc = 1; }
    return speed + inc;
}
console.log("inc (5,1) = " + inc(5, 1));
console.log("inc (5) = " + inc(5));
